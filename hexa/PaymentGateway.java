public interface PaymentGateway{
    public  abstract void transact(String from, String to, double amount, String remarks);
}