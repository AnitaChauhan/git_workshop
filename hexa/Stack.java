public class Stack{
    public static void main(String args[])
    {
        System.out.println("executed before calling method1");
        method1();
        System.out.println("executed after calling method1");
        
}

public static void method1()
{
    System.out.println("executed before calling method2");
    method2();
    System.out.println("executed after calling method2");

}

public static void method2()
{
    System.out.println("executed before calling method3");
    method3();
    System.out.println("executed after calling method3");

}

public static void method3()
{
    System.out.println("executed before calling method3");
    

}
}