public class Savings extends Banking1{
    public static double amount;

    @Override
    public void withdrawal(double cash)
    {
        if(cash>10000)
        {
          System.out.println("You need to maintain min balance");
        }
        else
        {
            System.out.println("Amount debited");
        }
        
    }
    @Override
    public void deposit(double money)
    {
        if(money>50000)
        {
            System.out.println("Max amount that can be deposited is Rs.50000");
        }
        else
        {
            System.out.println("Amount deposited");
        }
    }

    public double getAmount()
    {
        return amount;
    }
}