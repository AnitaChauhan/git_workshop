public class Account{
  private String name;
  private int id;
  private static int counter = 1000;

   Account(String name) {
       this.name=name;
       this.id = counter++;
   }

   public String getName(){
       return this.name;
   }

   public int getId(){
       return this.id;
   }
}